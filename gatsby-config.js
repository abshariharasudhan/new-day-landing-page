module.exports = {
  siteMetadata: {
    title: `Gatsby Default Starter`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
    OfferTitle:["EXPERT COUNSELLING","SCHOLARSHIP","GLOBAL CHOICE"],
    OfferDescription:["Get face-to-face meeting tailored to your needs from experts around the world","Attractive scholarships are available for the applicants","Choose from hundreds of college which suits to your needs"],
    mediaURL:["https://www.facebook.com/alphabsolutions/","https://twitter.com/alpha","https://www.linkedin.com/company/alpha-business-solutions-private-limited","https://www.skype.com/en/abs","https://www.instagram.com/alphababs"]
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/header-logo.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
