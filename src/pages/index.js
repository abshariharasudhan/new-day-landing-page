import React from "react"

import { Helmet } from "react-helmet"

import Layout from "../components/layout"
import Offers from "../components/offers/Offers"
import Banner from "../components/banner/Banner"

import Readmore from "../components/readMore/Read"
import Environment from "../components/environment/Environment"
import Testimonials from "../components/testimonials/Testimonials"
import Gallery from "../components/gallery/Gallery"

const IndexPage = () => (
  <Layout>
    <Helmet>
      <meta charSet="utf-8" />
      <title>New Day Education</title>
    </Helmet>
    <Banner />
    <Offers />
    <Environment />
    <Readmore />
    <Testimonials />
    <Gallery />
  </Layout>
)

export default IndexPage
