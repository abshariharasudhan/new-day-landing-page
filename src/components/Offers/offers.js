import React from 'react'

import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import commonStyles from "../../styles/common.module.css"
import styles from "./_offers.module.css"

export default () => {
  
  const data = useStaticQuery(graphql`
   query feedbackimages {
         site {
           siteMetadata {
            OfferTitle
            OfferDescription
           }
         }
      images: allFile(
        filter: { relativeDirectory: { eq: "offers_images" } }, sort: { fields: name, order: ASC }) {
        nodes {
          id
          childImageSharp {
            fixed(width: 70, height: 70) {
              ...GatsbyImageSharpFixed
            }
          }
        }
      }
    }
  `)

  var merged = data.site.siteMetadata.OfferTitle.map(function(_, i) {
    return [
      <Img fixed={data.images.nodes[i].childImageSharp.fixed} />,
      data.site.siteMetadata.OfferTitle[i],
      data.site.siteMetadata.OfferDescription[i],
    ]
  })

  
  return (
    <div className={commonStyles.container} id="whatdoweoffer">
      <div className={styles.container}>
      <div className={styles.heading}>
        <h2 style={{color: "#0a141e",fontWeight: "500"}}>What Do We Offer?</h2>
        <h5  className={styles.headingPara}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                  eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas
                  accumsan lacus vel facilisis.</h5>
        </div>
       <div className={styles.flex_container}>
        {merged.map((data,index) =>(
          <div className={styles.flex_wrapper} key={index}> 
            <div className={styles.img}>{data[0]}</div>
           <div className={styles.text}>{data[1]}
           <div className={styles.description}>{data[2]}</div>
           </div>
           
          </div>
         ))}
         </div>
      </div>
    </div>
  )
}
