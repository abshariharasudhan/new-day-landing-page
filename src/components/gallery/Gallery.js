import React from "react";
import { useStaticQuery, graphql } from "gatsby";
import GalleryImg from "gatsby-image";

import commonStyles from "../../styles/common.module.css";
import galleryStyles from "./_gallery.module.css";

const Gallery = () => {
  const data = useStaticQuery(graphql`
    query galleryImages {
      images: allFile(
        filter: { relativeDirectory: { eq: "gallery_images" } }
        sort: { fields: name, order: ASC }
      ) {
        nodes {
          id
          childImageSharp {
            fixed(width: 330, height: 200) {
              ...GatsbyImageSharpFixed
            }
          }
        }
      }
    }
  `)

  return (
    <div className={commonStyles.container} id="gallery">
      <div className={galleryStyles.total_gallery}>
        <h2 style={{ textAlign: "center", fontWeight: "500", color: "#0a141e" }}>Gallery</h2>
        <div className={galleryStyles.gallery_panel}>
          {data.images.nodes.map(image => (
            <div className={galleryStyles.image_style} key={image.id}>
              <GalleryImg
                fixed={image.childImageSharp.fixed}
                className={galleryStyles.image}
                style={{ display: "block" }}
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}
export default Gallery;
