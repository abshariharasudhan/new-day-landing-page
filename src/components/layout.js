import React, { useRef, useState, useEffect } from "react"

import Header from "./header/Header"
import Footer from "./footer/Footer"

const Layout = ({ children }) => {
  const LayoutRef = useRef()
  const [currentActive, setCurrentActive] = useState(0)

  useEffect(()=>{
    window.addEventListener("scroll", scrollEvent)

    return ()=>window.removeEventListener("scroll",scrollEvent)
  },[])

  const scrollEvent = (e) => {

    const LayoutCurrent = LayoutRef.current   // to get currently active section by checking the component's position
                                              // if currentActive is "5" then the scroll is on 6th component
    if((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      setCurrentActive(5)
    }
    else if(Math.floor(LayoutCurrent.children[5].getBoundingClientRect().y) <= 66 ) {
      setCurrentActive(4)
    }
    else if(Math.floor(LayoutCurrent.children[4].getBoundingClientRect().y) <= 66) {
      setCurrentActive(3)
    }
    else if(Math.floor(LayoutCurrent.children[3].getBoundingClientRect().y) <= 66) {
      setCurrentActive(3)
    }
    else if(Math.floor(LayoutCurrent.children[2].getBoundingClientRect().y) <= 66) {
      setCurrentActive(2)
    }
    else if(Math.floor(LayoutCurrent.children[1].getBoundingClientRect().y) <= 66) {
      setCurrentActive(1)
    }
    else {
      setCurrentActive(0)
    }
  }

  const onLink = (linkName, index) => {
    linkName = linkName.toLowerCase()
    linkName = linkName.replace(/ /g,"")    //remove white space
    linkName = linkName.replace("?","")
    window.location.hash = linkName
  }

  return (
    <>
      <Header 
      currentActive={currentActive} 
      onLink={onLink} 
      />
      <main ref={LayoutRef}>{children}</main>
      <Footer />
    </>
  )
}

export default Layout
