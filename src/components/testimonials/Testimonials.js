import React, { useState, useRef, useEffect } from "react"

import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import commonStyles from "../../styles/common.module.css"
import testimonialStyles from "./_testimonials.module.css"

const Testimonial = () => {
  const [slideWidth, setSlideWidth] = useState()
  const slideRef = useRef()
  const dotref = useRef()
  const counter = useRef(1)
  const slideInterval = useRef()
  let slideTotalLength = 5, size, sliding=0, startClientX, currentClientX;

  const data = useStaticQuery(graphql`
    query {
      profilepic: file(relativePath: { eq: "profilepic.jpg" }) {
        childImageSharp {
          fixed(width: 180, height: 180) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      quotes1: file(relativePath: { eq: "quotes1.png" }) {
        childImageSharp {
          fluid(maxWidth: 400) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      quotes2: file(relativePath: { eq: "quotes2.png" }) {
        childImageSharp {
          fluid(maxWidth: 400) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  useEffect(() => {
      
      setTimeout(()=>{
        getSlideWidth()
        //eslint-disable-next-line react-hooks/exhaustive-deps
        size = slideRef.current.children[0].clientWidth

        slideRef.current.style.transform = `translateX(${-size * counter.current}px)`
        dotref.current.children[counter.current - 1].classList.add(`${testimonialStyles.dot_active}`)
      
      },250)

    window.addEventListener("resize", (evt)=>{

      getSlideWidth()

      size = slideRef.current.children[0].clientWidth

      slideRef.current.style.transform = `translateX(${-size * counter.current}px)`

    })

    slideRef.current.addEventListener("mousedown", (evt)=>{
      mouseSlideStart(evt);
    })
    slideRef.current.addEventListener("mousemove", mouseSlide)
    slideRef.current.addEventListener("mouseup",mouseSlideEnd)

    // phone touch event
    slideRef.current.addEventListener("touchstart", (evt)=>{
      mouseSlideStart(evt);
    })
    slideRef.current.addEventListener("touchmove", mouseSlide)
    slideRef.current.addEventListener("touchend",mouseSlideEnd)

    window.addEventListener("transitionend", ()=>{
      transistionEnd()
      clearInterval(slideInterval.current)
      slideInterval.current = setInterval(()=>onSlideClick(counter.current+1), 4000)  // reset timer after transition end
    })
    //eslint-disable-next-line react-hooks/exhaustive-deps
    slideInterval.current = setInterval(()=>onSlideClick(counter.current+1), 4000)

    return ()=>{ 
      clearInterval(slideInterval.current)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const transistionEnd = () => {
    // From the below code the slide transforms from cloned slide to original without transistion
    if (counter.current === 0) {
      slideRef.current.style.transition = `none`
      counter.current = slideTotalLength - 2
      slideRef.current.style.transform = `translateX(${-size * counter.current}px)`
    }
    if (counter.current === slideTotalLength - 1) {
      slideRef.current.style.transition = `none`
      counter.current = slideTotalLength - counter.current
      slideRef.current.style.transform = `translateX(${-size * counter.current}px)`
    }
  }

  const getSlideWidth = () => {
    if(window.innerWidth > 1200) return setSlideWidth(1110)
    else if(window.innerWidth > 992) return setSlideWidth(930)
    else if(window.innerWidth > 767) return setSlideWidth(690)
    else if(window.innerWidth > 575) return setSlideWidth(510)
    else return setSlideWidth(window.innerWidth - 45)
  }

  const onSlideClick = (slideNo) => {
    if(counter.current >= slideTotalLength - 1 || counter.current <= 0){ 
      slideRef.current.style.transform = `translateX(${-size * counter.current}px)`
      return
    }
    size = slideRef.current.children[0].clientWidth;
    slideRef.current.style.transition = `transform 0.4s ease-in-out`
    dotref.current.children[counter.current-1].classList.remove(`${testimonialStyles.dot_active}`)
    counter.current = slideNo
    if (counter.current > slideTotalLength - 2) dotref.current.children[0].classList.add(`${testimonialStyles.dot_active}`)
    else if (counter.current <= 0) dotref.current.children[2].classList.add(`${testimonialStyles.dot_active}`)
    else dotref.current.children[slideNo-1].classList.add(`${testimonialStyles.dot_active}`)
    slideRef.current.style.transform = `translateX(${-size * counter.current}px)`
  }

  const mouseSlideStart = event => {
    if (sliding === 0) {
      if (counter.current !== 0 && counter.current !== 5 - 1) {
        startClientX = event.clientX
        sliding = 1
      }
    }
  }
  const mouseSlide = event => {
    event.preventDefault()
    if (event.which === 1) {
      if (sliding === 1) {
        slideRef.current.style.transition = `none`
        currentClientX = event.clientX - startClientX // calculate the movement of the mouse from start point
        slideRef.current.style.transform = `translateX(${-size * counter.current + currentClientX}px)`
      }
    }
  }
  const mouseSlideEnd = () => {
    if (sliding === 1) {
      slideRef.current.style.transition = `transform 0.4s ease-in-out`
      sliding = 0
      if (currentClientX <= -100) onSlideClick(counter.current+1)
      else if (currentClientX >= 100) onSlideClick(counter.current-1)
      else slideRef.current.style.transform = `translateX(${-size * counter.current}px)`
    }
  }

  return (
    <div className={testimonialStyles.background}>
      <div className={commonStyles.container}>
        <div className={testimonialStyles.wrapper}>
          <h1 className={testimonialStyles.heading}>Testimonials</h1>
          <div className={testimonialStyles.slide_container}>
            <div className={testimonialStyles.slide} style={{width:`${slideWidth * 5}px`}} ref={slideRef}>
            <div className={testimonialStyles.testimonial_wrapper} style={{width:`${slideWidth}px`}}>
                <div className={testimonialStyles.profile}>
                  <div className={testimonialStyles.image_container}>
                    <Img
                      fixed={data.profilepic.childImageSharp.fixed}
                      alt="Profile_pic"
                      className={testimonialStyles.img}
                    />
                  </div>
                  <div className={testimonialStyles.info}>
                    <p className={testimonialStyles.name}> Jane Doe</p>
                    <p className={testimonialStyles.place}>France</p>
                  </div>
                </div>
                <div className={testimonialStyles.quotes}>
                  <div className={testimonialStyles.quote1}>
                    <Img fluid={data.quotes1.childImageSharp.fluid} />
                  </div>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Molestie a iaculis at erat pellentesque adipiscing. Diam
                  phasellus vestibulum lorem sed. At augue eget arcu dictum.
                  <div className={testimonialStyles.quote2}>
                    <Img fluid={data.quotes2.childImageSharp.fluid} />
                  </div>
                </div>
              </div>
              <div className={testimonialStyles.testimonial_wrapper} style={{width:`${slideWidth}px`}}>
                <div className={testimonialStyles.profile}>
                  <div className={testimonialStyles.image_container}>
                    <Img
                      fixed={data.profilepic.childImageSharp.fixed}
                      alt="Profile_pic"
                      className={testimonialStyles.img}
                    />
                  </div>
                  <div className={testimonialStyles.info}>
                    <p className={testimonialStyles.name}> Jane Doe</p>
                    <p className={testimonialStyles.place}>France</p>
                  </div>
                </div>
                <div className={testimonialStyles.quotes}>
                  <div className={testimonialStyles.quote1}>
                    <Img fluid={data.quotes1.childImageSharp.fluid} />
                  </div>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Molestie a iaculis at erat pellentesque adipiscing. Diam
                  phasellus vestibulum lorem sed. At augue eget arcu dictum.
                  <div className={testimonialStyles.quote2}>
                    <Img fluid={data.quotes2.childImageSharp.fluid} />
                  </div>
                </div>
              </div>
              <div className={testimonialStyles.testimonial_wrapper} style={{width:`${slideWidth}px`}}>
                <div className={testimonialStyles.profile}>
                  <div className={testimonialStyles.image_container}>
                    <Img
                      fixed={data.profilepic.childImageSharp.fixed}
                      alt="Profile_pic"
                      className={testimonialStyles.img}
                    />
                  </div>
                  <div className={testimonialStyles.info}>
                    <p className={testimonialStyles.name}> Jane Doe</p>
                    <p className={testimonialStyles.place}>France</p>
                  </div>
                </div>
                <div className={testimonialStyles.quotes}>
                  <div className={testimonialStyles.quote1}>
                    <Img fluid={data.quotes1.childImageSharp.fluid} />
                  </div>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Molestie a iaculis at erat pellentesque adipiscing. Diam
                  phasellus vestibulum lorem sed. At augue eget arcu dictum.
                  <div className={testimonialStyles.quote2}>
                    <Img fluid={data.quotes2.childImageSharp.fluid} />
                  </div>
                </div>
              </div>
              <div className={testimonialStyles.testimonial_wrapper} style={{width:`${slideWidth}px`}}>
                <div className={testimonialStyles.profile}>
                  <div className={testimonialStyles.image_container}>
                    <Img
                      fixed={data.profilepic.childImageSharp.fixed}
                      alt="Profile_pic"
                      className={testimonialStyles.img}
                    />
                  </div>
                  <div className={testimonialStyles.info}>
                    <p className={testimonialStyles.name}> Jane Doe</p>
                    <p className={testimonialStyles.place}>France</p>
                  </div>
                </div>
                <div className={testimonialStyles.quotes}>
                  <div className={testimonialStyles.quote1}>
                    <Img fluid={data.quotes1.childImageSharp.fluid} />
                  </div>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Molestie a iaculis at erat pellentesque adipiscing. Diam
                  phasellus vestibulum lorem sed. At augue eget arcu dictum.
                  <div className={testimonialStyles.quote2}>
                    <Img fluid={data.quotes2.childImageSharp.fluid} />
                  </div>
                </div>
              </div>
              <div className={testimonialStyles.testimonial_wrapper} style={{width:`${slideWidth}px`}}>
                <div className={testimonialStyles.profile}>
                  <div className={testimonialStyles.image_container}>
                    <Img
                      fixed={data.profilepic.childImageSharp.fixed}
                      alt="Profile_pic"
                      className={testimonialStyles.img}
                    />
                  </div>
                  <div className={testimonialStyles.info}>
                    <p className={testimonialStyles.name}> Jane Doe</p>
                    <p className={testimonialStyles.place}>France</p>
                  </div>
                </div>
                <div className={testimonialStyles.quotes}>
                  <div className={testimonialStyles.quote1}>
                    <Img fluid={data.quotes1.childImageSharp.fluid} />
                  </div>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Molestie a iaculis at erat pellentesque adipiscing. Diam
                  phasellus vestibulum lorem sed. At augue eget arcu dictum.
                  <div className={testimonialStyles.quote2}>
                    <Img fluid={data.quotes2.childImageSharp.fluid} />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={testimonialStyles.dots} ref={dotref}>
            <button className={testimonialStyles.dot1} onClick={()=>onSlideClick(1)}></button>
            <button className={testimonialStyles.dot2} onClick={()=>onSlideClick(2)}></button>
            <button className={testimonialStyles.dot3} onClick={()=>onSlideClick(3)}></button>
          </div>
        </div>
      </div>
    </div>
  )
}
export default Testimonial