import React from "react"
import Img from "gatsby-image"
import { useStaticQuery, graphql } from "gatsby"

import commonStyles from "../../styles/common.module.css"
import environmentStyles from "../environment/_envirnoment.module.css"

const Environment = () => {
  const data = useStaticQuery(graphql`
    query Images {
      images: allFile(
        filter: { relativeDirectory: { eq: "environment_images" } }
        sort: { fields: name, order: ASC }
      ) {
        nodes {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  `)

  return (
      <div className={environmentStyles.bg} id="ourenvironment">
    <div className={commonStyles.container}>
      <div className={environmentStyles.main_div}>
        <h2 className={environmentStyles.h2}> Our Environment</h2>
        <div className={environmentStyles.main}>
          <div className={environmentStyles.card}>
            <Img
              className={environmentStyles.image}
              fluid={data.images.nodes[0].childImageSharp.fluid}
            />
            <h3 className={environmentStyles.h3}>Clean Campus</h3>
            <p className={environmentStyles.p}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
          </div>
          <div className={environmentStyles.card}>
            <Img
              className={environmentStyles.image}
              fluid={data.images.nodes[1].childImageSharp.fluid}
            />
            <h3 className={environmentStyles.h3}>Spacious Classrooms</h3>
            <p className={environmentStyles.p}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
          </div>
          <div className={environmentStyles.card}>
            <Img
              className={environmentStyles.image}
              fluid={data.images.nodes[2].childImageSharp.fluid}
            />
            <h3 className={environmentStyles.h3}>Vast Library</h3>
            <p className={environmentStyles.p}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
          </div>
        </div>
      </div>
    </div>
    </div>
  )
}

export default Environment
