import React from "react";

import { useStaticQuery, graphql } from "gatsby";
import SocialMediaImg from "gatsby-image";

import commonStyles from "../../styles/common.module.css";
import footerStyles from "./_footer.module.css";

const Footer = () => {
  const data = useStaticQuery(graphql`
    query socialMediaImages {
      site {
        siteMetadata {
          mediaURL
        }
      }
      images: allFile(
        filter: { relativeDirectory: { eq: "footer_images" } }
        sort: { fields: name, order: ASC }
      ) {
        nodes {
          id
          childImageSharp {
            fixed(width: 16, height: 16) {
              ...GatsbyImageSharpFixed
            }
          }
        }
      }
    }
  `)

  var mergedArr = data.site.siteMetadata.mediaURL.map(function (_, i) {
    return [
      <SocialMediaImg fixed={data.images.nodes[i].childImageSharp.fixed} />,
      data.site.siteMetadata.mediaURL[i],
    ]
  })

  return (
    <div className={footerStyles.background} id="contact">
      <div className={commonStyles.container}>
        <div className={footerStyles.footer_panel}>
          <div className={footerStyles.address_content}>
            <img
              src={require("../../images/footer-logo.png")}
              alt="footer-logo"
              className={footerStyles.foologo}
            />
            <p className={footerStyles.address_line}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt utlabore et dolore magna aliqua. Quis
              ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas
              accumsan lacus vel facilisis.
            </p>
          </div>

          <div className={footerStyles.second}>
            <div className={footerStyles.footer_address}>
              <p className={footerStyles.quick_underline}>
                <span>Quic</span>k Links
              </p>
              <ul className={footerStyles.address_support}>
                <li style={{ marginTop: "7px" }}>
                  <a href="#home">Home </a>
                </li>
                <li style={{ marginTop: "3px" }}>
                  <a href="#whatdoweoffer">
                    What Do We Offer?
                  </a>
                </li>
                <li style={{ marginTop: "3px" }}>
                  <a href="#ourenvironment">
                    Our Environment
                  </a>
                </li>
                <li style={{ marginTop: "3px" }}>
                  <a href="#whyus">
                    Why Us?
                  </a>
                </li>
                <li style={{ marginTop: "3px" }}>
                  <a href="#gallery">
                    Gallery
                  </a>
                </li>
                <li style={{ marginTop: "3px" }}>
                  <a href="#contact">
                    Contact
                  </a>
                </li>
              </ul>
            </div>

            <div>
              <p className={footerStyles.contact_info}>
                <span>Con</span>tact Info
              </p>
              <p className={footerStyles.address}>
              1329 Bryan Street <br />
                <span>North Grafton,</span><br />
                <span style={{ lineHeight: "0", marginTop: "-11px" }}>MA 01536</span>
              </p>
              <p style={{ fontSize: "14px", color: "white",fontWeight:"600" }}>
                203-752-0678 <br />
                <span>
                  <a href="mailto:newday@edu.com" className={footerStyles.link}>
                    contact@newday.edu
                  </a>
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className={footerStyles.media_area}>
        <div className={[commonStyles.container, footerStyles.media_img].join(" ")}>
          <p className={footerStyles.bottom_footer}>
            &copy; New Day Education. All Rights Reserved.
          </p>
          <div className={footerStyles.mediaImge}>
            {mergedArr.map((data,index) => (
              <a href={data[1]} style={{ marginLeft: "20px" }} key={index}>
                {data[0]}
              </a>
            ))}
          </div>
      </div>
      </div>
    </div>
)
}
export default Footer;