import React, { useEffect, useState, useRef } from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import PropTypes from "prop-types"

import { headerListOptions } from "./headerData"

import commonStyles from "../../styles/common.module.css"
import headerStyles from "./_header.module.css"

const Header = ({ currentActive, onLink }) => {
  const [isScrolled, setIsScrolled] = useState(false)
  const [showDropdown, setShowDropdown] = useState(true)
  const collaspsedMenuRef = useRef();

  const imageData = useStaticQuery(graphql`
    query {
      logoImage: file(relativePath: { eq: "header-logo.png" }) {
        childImageSharp {
          fixed(pngQuality: 10, width: 160) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  useEffect(() => {
    window.addEventListener("scroll", onScrolled)

    return () => window.removeEventListener("scroll", onScrolled)
  }, [])

  const onScrolled = () => {
    if (window.pageYOffset === 0) {
      setIsScrolled(false)
    } else {
      setIsScrolled(true)
    }
  }

  const onDropdownClick = () => {
    setShowDropdown(!showDropdown)
    if(showDropdown){
      collaspsedMenuRef.current.style.height = `${(headerListOptions.length * 65 )}px`
    }
    else {
      collaspsedMenuRef.current.style.height = `0`
    }

  }

  return (
    <header
      className={[
        headerStyles.container,
        isScrolled ? headerStyles.sticky_header : null,
      ].join(" ")}
    >
      <div className={[commonStyles.container, headerStyles.wrapper].join(" ")}>
        <div className={headerStyles.logo}>
          <Img
            fixed={imageData.logoImage.childImageSharp.fixed}
            style={{ display: "block" }}
          />
        </div>
        <div>
          <div className={headerStyles.linklists}>
            <ul>
              {headerListOptions.map((headerListOption, index) => (
                <li
                  className={[
                    headerStyles.linklist,
                    index === currentActive
                      ? headerStyles.linklist_active
                      : null,
                  ].join(" ")}
                  key={index}
                >
                  {" "}
                  {/* HeaderListOptions are loaded from "headerData.js" */}
                  <button onClick={()=>onLink(headerListOption.name, index)}>{headerListOption.name}</button>
                </li>
              ))}
            </ul>
          </div>
          <div className={headerStyles.collapsedlinklist}>
            <button
              className={[
                headerStyles.collapsedlinklist_btn,
                (!showDropdown)?headerStyles.collapsedlinklist_btn_expand:null
              ].join(" ")}
              onClick={onDropdownClick}
            >
              <span />
              <span />
              <span />
            </button>
            <nav
              className={[
                headerStyles.collapsedlinklist_dropdown,
              ].join(" ")}
              ref= {collaspsedMenuRef}
            >
              <ul>
                {headerListOptions.map((headerListOption, index) => (
                  <li className={""} key={index}>
                    {" "}
                    <button onClick={()=>onLink(headerListOption.name, index)} >{headerListOption.name}</button>
                  </li>
                ))}
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>
  )
}

Header.propTypes = {
  currentActive: PropTypes.number,
  onLink: PropTypes.func
}
export default Header
