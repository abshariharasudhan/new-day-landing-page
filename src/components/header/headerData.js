export const headerListOptions = [
  { name: "Home" },
  { name: "What Do We Offer?" },
  { name: "Our Environment" },
  { name: "Why Us?" },
  { name: "Gallery" },
  { name: "Contact" },
]
