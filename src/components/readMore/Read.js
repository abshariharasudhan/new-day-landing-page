import React from "react";
import { useStaticQuery, graphql } from "gatsby";
import Readimg from "gatsby-image";
import commonStyles from "../../styles/common.module.css";
import readStyles from "./_read.module.css";

export default () => {
    const data = useStaticQuery(graphql`
      query {
        readimg: file(relativePath: { eq: "read.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 500, maxHeight: 740) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `)
    return (
      <div className={commonStyles.container} id="whyus">
        <div className={readStyles.read_bg}>
         <div className={readStyles.read_container} style={{marginTop:"20px"}}>
            <div className={readStyles.readrow}>
            <div className={readStyles.col1}>
              <Readimg className={readStyles.readimg}
                fluid={data.readimg.childImageSharp.fluid}
                alt="read image"
              />
            </div>
          <div className={readStyles.col2}>
            <h2 className={readStyles.head}>Why Us?</h2>
          <div className={readStyles.paragraph_container}>
              <p className={readStyles.para1}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt utlabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
              <p className={readStyles.para2}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt utlabore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
          </div>
            <button type="button" className={readStyles.readbtn}>READ MORE</button>
          </div>
            </div>
        </div>
        </div>
      </div>
      )
  }