import React, { useState } from "react"

import commonStyles from "../../styles/common.module.css"
import bannerStyles from "./_banner.module.css"

export default () => {
  const [formName , setFormName] = useState()
  const [formEmail, setFormEmail] = useState()
  const [formPhone, setFormPhone] = useState("")
  const [formMessage, setFormMessage] = useState()

  const onNameValueChange = (e) => {
    var regex = /^[a-zA-Z ]*$/;
    if (e.target.value === '' || regex.test(e.target.value)) {
      setFormName(e.target.value)
   }
  }

  const onPhoneNumber = (e) => {
    var reg = /^[0-9 +]*$/;
    if (e.target.value === '' || reg.test(e.target.value)) {
      setFormPhone(e.target.value)
   }
  }

  const onEmailChange = (e) => {
    setFormEmail(e.target.value)
  }

 

  const onMessageValueChange = e => {
    setFormMessage(e.target.value)
  }


  return (
    <div className={bannerStyles.container} id="home">
      <div className={[commonStyles.container, bannerStyles.wrapper].join(" ")}>
        <div className={bannerStyles.grid}>
          <div className={bannerStyles.content}>
            <div>
              <h2>
                Premium WordPress <br />
                Landing Page
              </h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. In
                massa tempor nec feugiat nisl pretium fusce. Scelerisque purus
                semper eget duis at tellus at. Sagittis nisl rhoncus mattis
                rhoncus urna neque viverra.
              </p>
              <button>READ MORE</button>
            </div>
          </div>
          <div className={bannerStyles.form_container}>
            <div className={bannerStyles.form}>
              <form>
                <div className={bannerStyles.form_input_holder}>
                  <div className={bannerStyles.form_title}>
                    <h3>Let's get started</h3>
                    <p>
                      {" "}
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore.
                    </p>
                  </div>
                  <div className={bannerStyles.form_input}>
                    <input
                      placeholder="Name"
                      onChange={onNameValueChange}
                      name="name"
                      value={formName}
                    />
                    <input
                      type="email"
                      placeholder="Email"
                      onChange={onEmailChange}
                      name="email"
                      value={formEmail}
                    />
                    <input onInput={(e) => e.target.value = e.target.value.slice(0, 15)} placeholder="Phone Number" name="phoneNumber" onChange={onPhoneNumber} 
                    value={formPhone} />
                    <textarea
                      rows={3}
                      placeholder="Message"
                      onChange={onMessageValueChange}
                      name="message"
                      value={formMessage}
                    />
                  </div>
                </div>
                <button
                  className={bannerStyles.form_submit}
                  // onClick={onFormClick}
                >
                  Register Now
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
